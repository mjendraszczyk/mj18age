/**
 * Main js of module mjautocloseproduct
 * @author x13.pl.
 * @copyright (c) 2019, x13.pl
 * @license http://x13.pl x13.pl
 */

var Year;
var userYear;

function x13ageSetCookie(cname, cvalue, exdays, path) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path="+path;
}

function x13ageGetCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

$(document).ready(function () {

    if (!x13ageGetCookie('x13age')) {
        x13ageSetCookie('x13age', '0');
    }

    if (x13ageGetCookie('x13age') == '0') {
        $("#x13age").modal({
            backdrop: 'static',
            keyboard: false
        });
    }
    $("#addAgeCookie").click(function () {
        x13ageSetCookie("x13age", "1", 3, $("#x13age_uri").val());
    });
    $("#verifyAge").click(function () {
        console.log("test");
        x13ageVerifyAge();
    });
});




function x13ageVerifyAge() {
    Year = (new Date()).getTime();// get current time ; //getFullYear
    userYear = (new Date($('.age-confirmation #x13age select[name=year]').val() + '-' + $('.age-confirmation #x13age select[name=month]').val() + '-' + $('.age-confirmation #x13age select[name=day]').val())).getTime();
    date = new Date()
    Year18 = (new Date((date.getFullYear() - 18) + '-' + date.getMonth() + '-' + date.getDay())).getTime();

    if ((parseInt(Year) - userYear) >= parseInt(Year) - Year18) {
        $('#x13age').modal("hide");
        x13ageSetCookie("x13age", "1", 3, $("#x13age_uri").val());
        //console.log("ok");
    } else {
        $(".veryfication_info #wrong_date").css('display', 'block');
    }
}